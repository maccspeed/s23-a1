// Insert a single room
db.rooms.insertOne({
    "name": "single",
    "accomodates": 2,
    "price": 1000,
    "description": "A simple room with all the basic necessities",
    "rooms_available": 10,
    "isAvailable": false
});


// insert multiple rooms
db.rooms.insertMany([
{
    "name": "double",
    "accomodates": 3,
    "price": 2000,
    "description": "A room fit for a small family going on a vacation",
    "rooms_available": 5,
    "isAvailable": false
},
{
    "name": "double",
    "accomodates": 4,
    "price": 4000,
    "description": "A room with a queen size bed perfect for a simple getaway",
    "rooms_available": 15,
    "isAvailable": false
}
]);

// Use find method to search for a room with the name double
db.rooms.find({"name": "double"});

// Use updateOne method to update the queen room and set the available rooms to 0
db.rooms.updateOne(
{"description": "A room with a queen size bed perfect for a simple getaway"},
{$set:
    {
        "name": "queen",
        "rooms_available": 0
    }
}
);

// Use the deleteMany method rooms to delete all rooms that have 0 availability
db.rooms.deleteMany(
    {"rooms_available": 0}
);